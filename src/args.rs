//! Command line argument parsing

use std::path::PathBuf;

use argh::FromArgs;

/// Find the stats in new_stats that aren't in old_stats
#[derive(Debug, FromArgs)]
pub struct Args {
	/// old stats file
	#[argh(positional)]
	pub old_stats: PathBuf,

	/// new stats file
	#[argh(positional)]
	pub new_stats: PathBuf,
}
