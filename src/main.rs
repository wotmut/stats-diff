//! Find the stats in new_stats that aren't in old_stats

mod args;
mod stat;

use args::Args;
use stat::Stat;

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
	let args: Args = argh::from_env();

	let old_stats = Stat::from_json_file(&args.old_stats)?;

	let new_stats = Stat::from_json_file(&args.new_stats)?;

	let mut diff_stats: Vec<Stat> = Vec::new();

	for stat in new_stats {
		if !old_stats.contains(&stat) {
			diff_stats.push(stat.clone());
		}
	}

	println!("{}", serde_json::to_string_pretty(&diff_stats)?);

	Ok(())
}
