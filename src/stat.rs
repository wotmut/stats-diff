use std::fmt;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};
use serde_json::from_reader;

use crate::Result;

/// An individual record of a stat
#[derive(Clone, Debug, Default, Deserialize, PartialEq, Serialize)]
#[allow(non_snake_case)]
pub struct Stat {
	/// Class
	pub class: String,

	/// Con
	pub constitution: u8,

	/// Dex
	pub dexterity: u8,

	/// Homeland
	pub homeland: String,

	/// Stat identifier
	pub id: u128,

	/// Int
	pub intelligence: u8,

	/// Race
	pub race: String,

	/// Sex
	pub sex: String,

	/// Sum of stats
	pub statSum: u8,

	/// Str
	pub strength: u8,

	/// Date/time submitted
	pub submitted: String,

	/// Wil
	pub willpower: u8,
}

impl Stat {
	/// read stats from json file
	pub fn from_json_file<P: Into<PathBuf>>(p: P) -> Result<Vec<Self>> {
		let p: PathBuf = p.into();
		let f = File::open(&p).map_err(|e| format!("Unable to open file: {}", &e))?;
		let mut buf = BufReader::new(f);
		Ok(from_reader(&mut buf).map_err(|e| format!("Unable to read file: {}", &e))?)
	}
}

impl fmt::Display for Stat {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"['{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}],\n",
			self.submitted,
			self.race,
			self.class,
			self.sex,
			self.homeland,
			self.strength,
			self.intelligence,
			self.willpower,
			self.dexterity,
			self.constitution,
			self.statSum
		)
	}
}
